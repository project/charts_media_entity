<?php

namespace Drupal\charts_media_entity\BundlePlugin;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;

/**
 * Prevents uninstalling modules with bundle plugins in case of found data.
 */
class BundlePluginUninstallValidator implements ModuleUninstallValidatorInterface {

    /**
     * The entity type manager.
     *
     * @var EntityTypeManagerInterface $entityTypeManager
     */
    protected $entityTypeManager;

    /**
     * Constructs the object.
     *
     * @param EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager.
     */
    public function __construct(EntityTypeManagerInterface $entity_type_manager) {
        $this->entityTypeManager = $entity_type_manager;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($module) {
        $reasons = [];

        if ($module === 'charts_media_entity') {
            // Get media bundles that uses Chart media source
            $media_bundles = $this->entityTypeManager->getStorage('media_type')->loadByProperties([
                'source' => 'chart',
            ]);

            foreach ($media_bundles as $media_bundle) {
                // Load media entities of bundle that uses Chart media source
                $media_entities = \Drupal::entityTypeManager()->getStorage('media')->loadByProperties([
                    'bundle' => $media_bundle->id(),
                ]);

                // Add reason if there is a content of this bundle
                if (!empty($media_entities)) {
                    $reasons[] = t('There is data for the @bundle bundle on the @entity_type entity type. Please remove all content before uninstalling the module.', [
                        '@bundle' => $media_bundle->label(),
                        '@entity_type' => $media_bundle->getEntityType()->getLabel(),
                    ]);
                }
            }
        }

        return $reasons;
    }
}
