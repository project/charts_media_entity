<?php

namespace Drupal\charts_media_entity\Plugin\media\Source;

use Drupal\media\Annotation\MediaSource;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;

/**
 * HTML embed entity media source.
 *
 * @MediaSource(
 *   id = "chart",
 *   label = @Translation("Chart"),
 *   allowed_field_types = {"chart_config"},
 *   default_thumbnail_filename = "chart.png",
 *   description = @Translation("Provides business logic and metadata for Charts."),
 *   forms = {
 *     "media_library_add" = "\Drupal\charts_media_entity\Form\ChartForm"
 *   }
 * )
 */
class ChartSource extends MediaSourceBase {

  /**
   * {@inheritDoc}
   */
  public function getMetadataAttributes() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    switch ($attribute_name) {
      case 'default_name':
        return $media->uuid();

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

}
