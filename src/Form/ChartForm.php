<?php

namespace Drupal\charts_media_entity\Form;

use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\charts_media_entity\Plugin\media\Source\ChartSource;
use Drupal\media_library\Form\AddFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a chart form.
 */
class ChartForm extends AddFormBase {
  /**
   * Form Builder service definition.
   *
   * @var FormBuilderInterface $formBuilder
   */
  protected $formBuilder;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    $instance->formBuilder = $container->get('form_builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMediaType(FormStateInterface $form_state) {
    if ($this->mediaType) {
      return $this->mediaType;
    }

    $media_type = parent::getMediaType($form_state);
    if (!$media_type->getSource() instanceof ChartSource) {
      throw new \InvalidArgumentException('Can only add media types which use a "Charts Media Entity" source plugin.');
    }
    return $media_type;
  }

  /**
   * {@inheritDoc}
   */
  protected function buildInputElement(array $form, FormStateInterface $form_state) {
    $form['chart_settings'] = [
      '#type' => 'charts_settings',
    ];

    $form['chart_data'] = [
      '#type' => 'chart_data_collector_table',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#button_type' => 'primary',
      '#submit' => ['::addButtonSubmit'],
      '#ajax' => [
        'callback' => '::updateFormCallback',
        'wrapper' => 'media-library-wrapper',
        // @todo Remove when https://www.drupal.org/project/drupal/issues/2504115
        //   is fixed.
        'url' => Url::fromRoute('media_library.ui'),
        'options' => [
          'query' => $this->getMediaLibraryState($form_state)->all() + [
            FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function addButtonSubmit(array $form, FormStateInterface $form_state) {
    // Getting chart data and settings submitted in the form to construct a full chart field data
    $chart_data = $form_state->getValue('chart_data');
    $chart_settings = $form_state->getValue('chart_settings');

    // Attaching chart data to chart settings array
    $chart_settings['series'] = $chart_data;

    // Constructing full chart field data variable to create a chart media
    $full_chart = [
      'library' => $chart_settings["library"],
      'type' => $chart_settings["type"],
      'config' => $chart_settings
    ];

    // Processing data
    $this->processInputValues([$full_chart], $form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'charts_media_entity_chart_source';
  }
}
